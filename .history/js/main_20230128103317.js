function getID(x) {
  return document.getElementById(x);
}

function getTable() {
  var col = '',
    row = '',
    result = '';
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 20; j += 10) row += '    ' + j;
    col = '\r\n';
  }
  result = row;
  getID('txtGetTable').innerText = result;
}
