function getID(x) {
  return document.getElementById(x);
}

function getTable() {
  var col = 0,
    row = 0,
    result = 0;
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) row += '    ' + j;
    col = '<br>';
  }
  result = row + col;
  getID('txtGetTable').innerHTML = result;
}
