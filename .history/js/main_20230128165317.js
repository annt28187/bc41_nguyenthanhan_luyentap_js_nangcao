function getID(x) {
  return document.getElementById(x);
}

function getTable() {
  var result = '';
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) {
      result += `&nbsp;&nbsp;&nbsp;` + j;
    }
    result += `<br>`;
  }
  getID('txtGetTable').innerHTML = ` In bảng số: <br> ${result}`;
}

var arrNumber = [];

function getNumber() {
  var inputNumberEl = getEl('inputNumber').value * 1;
  arrNumber.push(inputNumberEl);
  getEl('txtArray').innerHTML = arrNumber;
}
