function getID(x) {
  return document.getElementById(x);
}

// Bài 01.
function getTable() {
  var result = '';
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) {
      result += `&nbsp;&nbsp;&nbsp;` + j;
    }
    result += `<br>`;
  }
  getID('txtGetTable').innerHTML = ` In bảng số: <br> ${result}`;
}

// Bài 02.
var arrNumber = [];

function getNumber() {
  var inputNumber1El = getID('inputNumber1').value * 1;
  arrNumber.push(inputNumber1El);
  getID('txtArray').innerHTML = arrNumber;
}

function checkPrime(n) {
  if (n < 2) return false;
  for (var r = 2; r <= Math.sqrt(n); r++) {
    if (n % r == 0) return false;
  }
  return true;
}

function printPrime() {
  var result = [];
  for (var i = 0; i < arrNumber.length; i++) {
    if (checkPrime(arrNumber[i])) result.push(arrNumber[i]);
  }
  getID('txtPrintPrime').innerHTML = `Số nguyên tố trong mảng: ${result}`;
}

// Bài 03.
function sumNumber() {
  var inputNumber2El = getID('inputNumber2').value * 1;
  var result = 0,
    sum = 0;
  if (inputNumber2El < 2) {
    getID('txtSumNumber').innerHTML = `Nhập số nguyên lớn hơn hoặc bằng 2`;
  } else {
    for (var i = 2; i <= inputNumber2El; i++) {
      sum += i;
    }
    result = sum + 2 * inputNumber2El;
    getID('txtSumNumber').innerHTML = `Tính tổng S = ${result}`;
  }
}

// Bài 04.
function countDivisor() {
  var inputNumber3El = getID('inputNumber3').value * 1;
  var count = 0,
    divisorArr = [];
  if (inputNumber3El < 0) {
    getID('txtCountDivisor').innerHTML = `Nhập số nguyên lớn hơn 0`;
  } else {
    for (var i = 1; i <= inputNumber3El; i++) {
      if (inputNumber3El % i == 0) {
        count += 1;
        divisorArr.push(i);
      }
    }
    getID(
      'txtCountDivisor'
    ).innerHTML = ` Số lượng ước số của ${inputNumber3El} là ${count}<br>👉 Ước số của ${inputNumber3El} là ${divisorArr}`;
  }
}

// Bài 05.
function reverseNum(n) {
  n = n + '';
  return n.split('').reverse().join('');
}

function printReverseNum() {
  var inputNumber4El = getID('inputNumber4').value * 1;
  var result = 0;
  if (inputNumber4El < 0) {
    getID('txtPrintReverseNum').innerHTML = `Nhập số nguyên lớn hơn 0`;
  } else {
    result = Number(reverseNum(inputNumber4El));
    getID('txtPrintReverseNum').innerHTML = ` Số đảo ngược của ${inputNumber4El} là ${result}`;
  }
}

// Bài 06.
function findMaxX() {
  var result = 0,
    sum = 0;
  for (var i = 1; sum + i <= 100; i++) {
    sum += i;
    result = i;
  }
  getID(
    'txtFindMaxX'
  ).innerHTML = ` Tổng từ 1 đến ${result} là ${sum}<br>👉 Giá trị của X là ${result}`;
}

// Bài 07.
function printMultiTableN() {
  var inputNumber5El = getID('inputNumber5').value * 1;
  var result = '';
  for (var i = 0; i <= 10; i++) {
    result += `${inputNumber5El} &times; ${i} &equals; ${inputNumber5El * i}<br>`;
  }
  getID('txtPrintMultiTableN').innerHTML = ` Bảng cửu chương của ${inputNumber5El}là<br> ${result}`;
}

// Bài 08.
function dealingCards() {
  var cards = ['4K', 'KH', '5C', 'KA', 'QH', 'KD', '2H', '10S', 'AS', '7H', '9K', '10D'];
}

// Bài 09.
function calChickenDog() {
  var inputSumEl = getID('inputSum').value * 1;
  var inputFootEl = getID('inputFoot').value * 1;
  var numChicken = 0,
    numDog = 0;

  if (inputSumEl < 0 || inputFootEl < 0) {
    getID('txtCalChickenGog').innerHTML = `Nhập số tổng số gà chó và tổng số chân lớn hơn 0`;
  } else {
    if (inputFootEl % 2 != 0) {
      getID('txtCalChickenGog').innerHTML = `Nhập số tổng số chân là số chẳn`;
    } else {
      var max = Math.round(inputFootEl / 4) * 1;
      var min = Math.round(inputSumEl / 4) * 1;
      for (var i = min; i < max; i++) {
        if (i * 2 + (inputSumEl - i) * 4 == inputFootEl) {
          numChicken = i;
          numDog = inputSumEl - i;
        }
      }
      getID('txtCalChickenGog').innerHTML = ` Số gà là ${numChicken}<br>👉 Số chó là ${numDog}`;
    }
  }
}

// Bài 10.
function calDeflectionAngle() {
  var inputHourEl = getID('inputHour').value * 1;
  var inputMinuteEl = getID('inputMinute').value * 1;
  var result = Math.abs(inputMinuteEl * 6 - 0.5 * (inputHourEl * 60 + inputMinuteEl)) * 1;
  getID(
    'txtCalDeflectionAngle'
  ).innerHTML = ` Góc lệnh giữa ${inputHourEl} giờ và ${inputMinuteEl} phút là ${result}<sup>0</sup>`;
}
