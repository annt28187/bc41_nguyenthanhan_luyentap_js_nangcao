var arrNumber = [],
  arrFloat = [];

function getEl(val) {
  return document.getElementById(val);
}

function getNumber() {
  var inputNumberEl = getEl("inputNumber").value * 1;
  arrNumber.push(inputNumberEl);
  getEl("txtArray").innerHTML = arrNumber;
}

function sumPositive() {
  var sum = 0,
    current;
  for (var i = 0; i < arrNumber.length; i++) {
    current = arrNumber[i];
    if (current > 0) {
      sum += current;
    }
  }
  getEl("txtSum").innerHTML = `Tổng số dương: ${sum}`;
}

function countPositive() {
  var count = 0,
    current;
  for (var i = 0; i < arrNumber.length; i++) {
    current = arrNumber[i];
    if (current > 0) {
      count++;
    }
  }
  getEl("txtCount").innerHTML = `Đếm số dương: ${count}`;
}

function findMin() {
  var min = arrNumber[0];
  for (var i = 1; i < arrNumber.length; i++) {
    if (arrNumber[i] < min) {
      min = arrNumber[i];
    }
  }
  getEl("txtMin").innerHTML = `Số nhỏ nhất: ${min}`;
}

function findMinPos() {
  var arrTemp = [];
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] > 0) {
      arrTemp.push(arrNumber[i]);
    }
  }
  if (arrTemp.length > 0) {
    var min = arrTemp[0];
    for (var i = 1; i < arrTemp.length; i++) {
      if (arrTemp[i] < min) {
        min = arrTemp[i];
      }
    }
    getEl("txtMinPos").innerHTML = `Số dương nhỏ nhất: ${min}`;
  } else {
    getEl("txtMinPos").innerHTML = `Không có số dương trong mảng`;
  }
}

function findEven() {
  var even = 0;
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] % 2 == 0) {
      even = arrNumber[i];
    } else {
      even = -1;
    }
  }
  getEl("txtEven").innerHTML = `Số chẵn cuối cùng: ${even}`;
}

function swap(a, b) {
  var tmp = arrNumber[a];
  arrNumber[a] = arrNumber[b];
  arrNumber[b] = tmp;
}

function changePosition() {
  var inputIndex1El = document.getElementById("inputIndex1").value * 1;
  var inputIndex2El = document.getElementById("inputIndex2").value * 1;
  swap(inputIndex1El, inputIndex2El);
  getEl("txtChangePos").innerHTML = `Mảng sau khi đổi vị trí: ${arrNumber}`;
}

function sortIncrease() {
  for (var i = 0; i < arrNumber.length; i++) {
    for (var j = 0; j < arrNumber.length - 1; j++) {
      if (arrNumber[j] > arrNumber[j + 1]) {
        swap(j, j + 1);
      }
    }
  }
  getEl(
    "txtIncrease"
  ).innerHTML = `Mảng sau khi sắp xếp tăng dần: ${arrNumber}`;
}

function checkPrime(n) {
  if (n < 2) return false;
  for (var r = 2; r <= Math.sqrt(n); r++) {
    if (n % r == 0) return false;
  }
  return true;
}

function findPrime() {
  var result = 0;
  for (var i = 0; i < arrNumber.length; i++) {
    if (checkPrime(arrNumber[i])) {
      result = arrNumber[i];
      break;
    } else {
      result = -1;
    }
  }
  getEl("txtPrime").innerHTML = `Số nguyên tố đầu tiên trong mảng: ${result}`;
}

function getFloat() {
  var inputFloatEl = getEl("inputFloat").value * 1;
  arrFloat.push(inputFloatEl);
  getEl("txtArrayFloat").innerHTML = arrFloat;
}

function findInt() {
  var count = 0;
  for (var i = 0; i < arrFloat.length; i++) {
    if (Number.isInteger(arrFloat[i])) {
      count++;
    }
  }
  getEl("txtInt").innerHTML = `Số nguyên: ${count}`;
}

function compareNum() {
  var positive = 0,
    negative = 0;
  for (var i = 0; i < arrNumber.length; i++) {
    if (arrNumber[i] < 0) negative++;
    if (arrNumber[i] > 0) positive++;
  }
  getEl("txtCompare").innerHTML =
    positive > negative
      ? "Số dương > Số âm"
      : positive < negative
      ? "Số âm > Số dương"
      : "Số âm = Số dương";
}
