function getID(x) {
  return document.getElementById(x);
}

// Bài 01.
function getTable() {
  var result = '';
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) {
      result += `&nbsp;&nbsp;&nbsp;` + j;
    }
    result += `<br>`;
  }
  getID('txtGetTable').innerHTML = ` In bảng số: <br> ${result}`;
}

// Bài 02.
var arrNumber = [];

function getNumber() {
  var inputNumberEl = getID('inputNumber').value * 1;
  arrNumber.push(inputNumberEl);
  getID('txtArray').innerHTML = arrNumber;
}

function checkPrime(n) {
  if (n < 2) return false;
  for (var r = 2; r <= Math.sqrt(n); r++) {
    if (n % r == 0) return false;
  }
  return true;
}

function printPrime() {
  var result = [];
  for (var i = 0; i < arrNumber.length; i++) {
    if (checkPrime(arrNumber[i])) result.push(arrNumber[i]);
  }
  getID('txtPrintPrime').innerHTML = `Số nguyên tố trong mảng: ${result}`;
}

// Bài 03.
function sumNumber() {
  var inputNumEl = getID('inputNum').value * 1;
  var result = 0;
  if (inputNumEl < 2) {
    getID('txtSumNumber').innerHTML = `Nhập số nguyên lớn hơn hoặc bằng 2`;
  } else {
    for (var i = 1; i <= inputNumEl; i++) {
      result = result + 3(i + 3);
    }
    getID('txtSumNumber').innerHTML = `Tính tổng S = ${result}`;
  }
}
