function getID(x) {
  return document.getElementById(x);
}

function getTable() {
  var col = '',
    row = '',
    result = '';
  for (var i = 1; i <= 10; i++) {
    for (var j = i; j <= 100; j += 10) {
      row += '    ' + j;
      if (row.length == 10) {
        col += '<br>';
      }
    }
  }
  result = row + col;
  getID('txtGetTable').innerHTML = ` In bảng số: <br> ${result}`;
}
